import React from 'react';
import { getIcon } from '../../helpers/getImgSrc.js';
// STYLES
import "./styles.css"

function PageButtons() {
    return (
        <div className='page-buttons'>
            <button className="select-page">
                <img src={getIcon("about")} alt="" width={30} height={30} />
            </button>
            <button className="select-page">
                <img src={getIcon("academic")} alt="" width={30} height={30} />
            </button>
            <button className="select-page">
                <img src={getIcon("work")} alt="" width={30} height={30} />
            </button>
            <button className="select-page">
                <img src={getIcon("project")} alt="" width={30} height={30} />
            </button>
            <button className="select-page">
                <img src={getIcon("certificate")} alt="" width={30} height={30} />
            </button>
            <button className="select-page">
                <img src={getIcon("hobbies")} alt="" width={30} height={30} />
            </button>
            
        </div>
    );
}

export default PageButtons;
