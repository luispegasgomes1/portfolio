import React from 'react';
// Images
import GitImg from "../../assets/icons/github.png"
import HireImg from "../../assets/icons/hiring.png"
import LinkedInImg from "../../assets/icons/linkedin.png"
// STYLES
import "./styles.css"

function ActionsBtns() {
    return (
        <div className='actions-btns'>
            <a href="https://github.com/luispegasgomes">
                <button className="bgBeige">
                    <img src={GitImg} width="30" />
                </button>
            </a>
            <a href="mailto:luispegasgomes@gmail.com">
                <button className="bgDarkBlue fontBarlow hire-me">
                    <img src={HireImg} width="30" />
                    <span className="colorWhite">Hire me!</span>
                </button>
            </a>
            <a href="https://www.linkedin.com/in/lu%C3%ADs-gomes-969531207/">
                <button className="bgBeige">
                    <img src={LinkedInImg} width="30" />
                </button>
            </a>
        </div>
    )
}
export default ActionsBtns;
