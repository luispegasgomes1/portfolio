import React from 'react';
// STYLES
import './App.css';
// PAGES
import Home from './pages/Home.jsx';

function App() {
  return (
    <div className="App">
      <Home/>
    </div>
  );
}

export default App;
