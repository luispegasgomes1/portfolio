import React from 'react';
// Images
import ProfileImg from "../assets/luisgomes.jpg"
import WheelImg from "../assets/icons/wheel.png"
// COMPONENTS
import ActionsBtns from '../components/actions-btns/actions-btns.jsx';
import PageButtons from '../components/page-buttons/page-buttons.jsx';

function Home() {
    return (
        <div className='home'>
            <h1 className="fontAsap color-dark-blue animate-charcter mt-4 title">
                <span className='colorWhite'>I'm </span>
                Luís G<img className="rotate-rim" src={WheelImg} alt="wheel-icon" width="32px" />mes
            </h1>
            <p className="job-description fontBarlow color-dark-blue" >
                Software Engineer<br />
                Junior at MOG Technologies
            </p>
            <img src={ProfileImg} className='profile-image' alt="profile-image" width="220vh" height="220vh" />
            <ActionsBtns />
            <hr />
            <PageButtons />
        </div>
    );
}

export default Home;
